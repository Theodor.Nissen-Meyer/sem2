# Connect Four

### Hvordan man spiller

Spillet spilles mellom to brukere, Rød og Gul. Rød begynner.

Spiller 1 plasserer sin første brikke på brettet. Brikken vil falle ned til nederste ledige plass. Det er nå spiller to sin tur. En vinner kåres når en av spillerene har fire brikker av sin farge på rad, enten horisontalt, vertikalt eller diagonalt.

## AI motstander
Spillet har innebygget AI-funksjon. For å aktivere denne, må GameState-en settes til 'GameState.AI_ACTIVE'. For two-player: 'GameState.ACTIVE_GAME'. Pass på! Det er fort gjort å overse muligheter :)

Lenke til videodemonstrasjon:
https://youtu.be/pgKkd06IBVo



